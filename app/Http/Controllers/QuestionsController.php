<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use \Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class QuestionsController extends Controller
{


  public function index() {
    $questions = Test::orderBy('id','desc')->paginate(20);
    return view('questions.index')
        ->with('questions', $questions);
  }

  public function create($id) {
    return view('questions.create')->with('test_id', $id);
  }

  public function store($id, Request $request) {
    $this->validate($request, [
        'question' => 'required'
    ]);
    $question = new Question;
    $question->question = $request->question;
    $question->description = $request->description;
    $question->test_id = $id;

    if ($request->hasFile('image')) {

      $file = $request->file('image');
      $path = $file->hashName('uploads');
      $image = Image::make($file);
      $image->fit(300, 200);
      $info = pathinfo($path);
      $path = $info['dirname'].'/'.$info['filename'].'.jpg';
      Storage::put($path, (string) $image->encode('jpg'));
      $question->image = $path;
    }

    $question->save();
    return redirect()->route('tests.edit', ['id' => $id]);
  }

  public function edit($id) {
    $question = Question::find($id);
    return view('questions.edit')
        ->with('question', $question);
  }

  public function update(Request $request, $id)
  {
      $question = Question::find($id);
      $question->question = $request->question;
      $question->description = $request->description;

      if ($request->hasFile('image')) {
          if (Storage::exists($question->image)) {
             Storage::delete($question->image);
          }
          $file = $request->file('image');
          $path = $file->hashName('uploads');
          $image = Image::make($file);
          $image->fit(300, 200);
          //$image->mask('images/mask.png', true);
          $info = pathinfo($path);
          $path = $info['dirname'].'/'.$info['filename'].'.jpg';
          Storage::put($path, (string) $image->encode('jpg'));
          $question->image = $path;
      }
      $question->save();
      return redirect()->route('tests.edit', ['id' => $question->test_id]);
  }

  public function delete($id) {
    $question = Question::find($id);
    $question->delete();
    return redirect()->back();
  }

}
