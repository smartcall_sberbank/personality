<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helldar\Vk\Models\VkUser;
use Helldar\Vk\Models\VkRequest;
use App\User;
use \Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class VkUsersController extends Controller
{


  public function index() {
    $users = User::orderBy('id','desc')->paginate(20);
    return view('vkusers.list')
        ->with('users', $users);
  }

  public function groups($id) {
    $user = User::find($id);
    return view('vkusers.groups')->with('user', $user);
  }

  public function friends($id) {
    $user = User::find($id);
    return view('vkusers.friends')->with('user', $user);
  }

  public function groupsnow($id) {


    $item = VkRequest::withTrashed()->whereUserId($id)->whereMethod('groups.get')->first();

    if(!isset($item)) {

      vk()->groups('get')->userId($id)->extended(1)->send();

      $response = vk()->groups('get')->get($id);
    } else {

      $response = $item->response;
    }

    return view('vkusers.groupsnow')->with('response', $response);
  }

}
