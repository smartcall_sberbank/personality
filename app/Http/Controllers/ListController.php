<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Test;
use Illuminate\Support\Facades\Session;


class ListController extends Controller
{
  public function index()
  {
      $categories = Category::all();
      return view('home')->with(['categories' => $categories]);
  }

  public function test($id)
  {

    $others = Test::all()->take(7);

    $test = Test::find($id);
    Session::put('test_id', $id);
    Session::save();
    return view('list.test')
        ->with('test', $test)
        ->with('others', $others);
  }

  public function testQuestions($id)
  {

    $others = Test::all()->take(7);

    $test = Test::find($id);
    return view('list.testquestions')
        ->with('test', $test)
        ->with('others', $others);
  }

  public function category($id)
  {
      $category = Category::find($id);
      return view('list.category')->with(['category' => $category]);
  }


}
