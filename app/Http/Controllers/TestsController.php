<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use \Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TestsController extends Controller
{


  public function index() {
    $tests = Test::orderBy('id','desc')->paginate(20);
    return view('tests.index')
        ->with('tests', $tests);
  }

  public function create() {
    return view('tests.create');
  }

  public function store(Request $request) {
    $this->validate($request, [
        'name' => 'required'
    ]);
    $test = new Test;
    $test->name = $request->name;
    $test->description = $request->description;
    $test->category_id = $request->category_id;

    if ($request->hasFile('image')) {

      $file = $request->file('image');
      $path = $file->hashName('uploads');
      $image = Image::make($file);
      $image->fit(600, 400);
      $info = pathinfo($path);
      $path = $info['dirname'].'/'.$info['filename'].'.jpg';
      Storage::put($path, (string) $image->encode('jpg'));
      $test->image = $path;
    }

    $test->save();
    return redirect()->route('tests.all');
  }

  public function edit($id) {
    $test = Test::find($id);
    return view('tests.edit')
        ->with('test', $test);
  }

  public function update(Request $request, $id)
  {
      $test = Test::find($id);
      $test->name = $request->name;
      $test->description = $request->description;
      $test->category_id = $request->category_id;

      if ($request->hasFile('image')) {
          if (Storage::exists($test->image)) {
             Storage::delete($test->image);
          }
          $file = $request->file('image');
          $path = $file->hashName('uploads');
          $image = Image::make($file);
          $image->fit(600, 400);
          //$image->mask('images/mask.png', true);
          $info = pathinfo($path);
          $path = $info['dirname'].'/'.$info['filename'].'.jpg';
          Storage::put($path, (string) $image->encode('jpg'));
          $test->image = $path;
      }
      $test->save();
      return redirect()->route('tests.all');
  }



}
