<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Variant;
use \Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class VariantsController extends Controller
{


  public function create($id) {
    return view('variants.create')->with('question_id', $id);
  }

  public function store($id, Request $request) {
    // $this->validate($request, [
    //     'text' => 'required'
    // ]);
    $variant = new Variant;
    $variant->text = $request->text;
    $variant->question_id = $id;

    if ($request->hasFile('image')) {

      $file = $request->file('image');
      $path = $file->hashName('uploads');
      $image = Image::make($file);
      $image->fit(300, 200);
      $info = pathinfo($path);
      $path = $info['dirname'].'/'.$info['filename'].'.jpg';
      Storage::put($path, (string) $image->encode('jpg'));
      $variant->image = $path;
    }

    $variant->save();
    return redirect()->route('questions.edit', ['id' => $id]);
  }

  public function edit($id) {
    $variant = Variant::find($id);
    return view('variants.edit')
        ->with('variant', $variant);
  }

  public function update(Request $request, $id)
  {
      $variant = Variant::find($id);
      $variant->text = $request->text;


      if ($request->hasFile('image')) {
          if (Storage::exists($variant->image)) {
             Storage::delete($variant->image);
          }
          $file = $request->file('image');
          $path = $file->hashName('uploads');
          $image = Image::make($file);
          $image->fit(300, 200);
          //$image->mask('images/mask.png', true);
          $info = pathinfo($path);
          $path = $info['dirname'].'/'.$info['filename'].'.jpg';
          Storage::put($path, (string) $image->encode('jpg'));
          $variant->image = $path;
      }
      $variant->save();
      return redirect()->route('questions.edit', ['id' => $variant->question_id]);
  }

  public function delete($id) {
    $variant = Variant::find($id);
    $variant->delete();
    return redirect()->back();
  }

}
