<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
  public $timestamps = false;
  
  public function test()
  {
    return $this->belongsTo('App\Question');
  }

}
