<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
  public function questions()
  {
    return $this->hasMany('App\Question');
  }

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

}
