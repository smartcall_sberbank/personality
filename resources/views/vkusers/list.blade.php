@extends('layouts.main')

@section('content')


        <h2 class="page-header">Пользователи</h2>


          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Аккаунт</th>
                  <th>email</th>
                  <th>Группы</th>
                  <th>Друзья</th>

                </tr>
              </thead>
              <tbody>
                  @foreach($users as $user)
                <tr>
                  <td>{{ $user->id }}</td>
                  <td><a target="_blank" href="https://vk.com/id{{ $user->vk_user_id }}">{{ $user->vk_user_id }}</a></td>
                  <td>{{ $user->email }}</td>
                  <td><a href="{{ route('vkusers.groups', ['id' => $user->id]) }}">подробнее</a></td>
                  <td><a href="{{ route('vkusers.friends', ['id' => $user->id]) }}">подробнее</a></td>


                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        <?php echo $users->render(); ?>

@endsection
