@extends('layouts.main')

@section('content')


<h2 class="page-header">Пользователи {{ $user->vk_user_id }}</h2>

<?php
$friends = $user->requests->where('method', 'friends.get')->first();
$list = json_decode($friends->response);
//dd($list)
 ?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Изображение</th>
                  <th>Имя</th>
                  <th>Никнейм</th>
                  <th>Пол</th>
                  <th>Страна</th>
                  <th>Город</th>
                  <th>Образование</th>
                  <th>Группы</th>
                  <th>Контакт</th>


                </tr>
              </thead>
              <tbody>
@foreach(array_slice($list->response->items,400) as $item)

@if ($item->first_name != 'DELETED')
<tr>
  <td>{{ $item->id }}</td>
    <td><img src="{{ $item->photo_50 }}"></td>
  <td>{{ $item->first_name }} {{ $item->last_name }}</td>
  <td>@if(isset($item->nickname)){{ $item->nickname }}@endif</td>
  <td>@if(isset($item->sex)){{ $item->sex }}@endif</td>
  <td>@if(isset($item->country->title)){{ $item->country->title }}@endif</td>
  <td>@if(isset($item->city->title)){{ $item->city->title }}@endif</td>
  <td>@if(isset($item->university_name)){{ $item->university_name }}@endif</td>
  <td><a href="{{ route('vkusers.groupsnow', ['id' => $item->id]) }}">запрос</a></td>
  <td>@if(isset($item->home_phone)){{ $item->home_phone }}@endif</td>
</tr>
@endif
@endforeach

              </tbody>
            </table>
          </div>


@endsection
