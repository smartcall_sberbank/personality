@extends('layouts.main')

@section('content')


        <h2 class="page-header"> Группы пользователя</h2>

<?php
//$groups = $user->requests->where('method', 'groups.get')->first();
$list = json_decode($response);

 ?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Изображение</th>
                  <th>Название</th>
                  <th>Алиас</th>


                </tr>
              </thead>
              <tbody>
@foreach($list->response->items as $item)

<tr>
  <td>{{ $item->id }}</td>
    <td><img src="{{ $item->photo_50 }}"></td>
  <td>{{ $item->name }}</td>
  <td>{{ $item->screen_name }}</td>

</tr>
@endforeach

              </tbody>
            </table>
          </div>


@endsection
