@extends('layouts.front')

@section('content')
<section class="page-wrap">
	<div class="wrapper">
		<ul class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active"></li>
		</ul>
		<ul class="social-networks">
			<li><a href="#"><img src="/images/icon_facebook.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon-twitter.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_pinterest.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_google.png" alt=""></a></li>
		</ul>

@foreach ($categories as $category)

		<div class="s-test-list popular-tests">
			<h3>{{ $category->name }}</h3>
			<div class="row">

        @foreach ($category->tests->take(10) as $test )
				<div class="col-xs-6 col-sm-3 col-md-2">
					<div class="test-el">
						<div class="image"><a href="<?php echo route('test', ['id' => $test->id]); ?>"><img src="/images/{{ $test->image }}" alt=""></a></div>
						<div class="title"><a href="<?php echo route('test', ['id' => $test->id]); ?>">{{ $test->name }}</a></div>
						<div class="desc">{{ $test->description }}</div>
					</div>
				</div>

        @endforeach

			</div>
			<a class="link-more" href="<?php echo route('category', ['id' => $category->id]); ?>">ЕЩЕ</a>
		</div>
		<div class="wrapper">
			<div class="gray-line"></div>
		</div>

@endforeach



	</div>
</section>



@endsection
