@extends('layouts.main')

@section('content')
<h2 class="page-header">Редактирование вопроса #{{ $question->id }}</h2>

<form autocomplete="off" enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo route('questions.update', ['id' => $question->id]) ?>">
        {{ csrf_field() }}


<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="account" class="col-sm-2 control-label">Наименование</label>
    <div class="col-sm-4">
        <input class="form-control" id='question' type="text" value="{{ $question->question }}" name="question">
            @if ($errors->has('question'))
                <span class="help-block">
                    <strong>{{ $errors->first('question') }}</strong>
                </span>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="account" class="col-sm-2 control-label">Описание</label>
    <div class="col-sm-4">
        <textarea name="description" rows="8" cols="80">{{ $question->description }}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label for="image" class="col-sm-2 control-label">Изображение</label>
    <div class="col-sm-4">
        <input id='image' type="file" value="" name="image">
    </div>
</div>
@if ($question->image)
<div class="form-group">
    <img src="/images/{{ $question->image }}">
</div>
@endif


<h4 class="page-header">Варианты ответа</h4>
<div class="pull-right"><a class="btn btn-default" href="<?php echo route('variants.create',  ['id' => $question->id]) ?>" role="button">Добавить</a></div>

  <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Наименование</th>
          <th>Описание</th>
          <th>Изображение</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
          @foreach($question->variants as $variant)
        <tr>
          <td>{{ $variant->id }}</td>
          <td>{{ $variant->text }}</td>
          <td>{{ $variant->description }}</td>
          <td>@if($variant->image)<img src="/images/{{ $variant->image }}" height="60">@else &nbsp; @endif</td>

          <td><a class="btn btn-default btn-sm" href="<?php echo route('variants.edit', ['id' => $variant->id]); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
          <a class="btn btn-default btn-sm" href="<?php echo route('variants.delete', ['id' => $variant->id]); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
        </tr>
        @endforeach

      </tbody>
    </table>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-2">
      <button type="submit" class="btn btn-default">Сохранить</button>
    </div>
  </div>
</form>

@endsection
