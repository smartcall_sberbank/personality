@extends('layouts.main')

@section('content')


        <h2 class="page-header">Тесты</h2>
        <div class="pull-right"><a class="btn btn-default" href="<?php echo route('tests.create') ?>" role="button">Добавить</a></div>
        <div class="row">
            <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="<?php echo route('tests.all') ?>">Все</a></li>

            </ul>
        </div>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Наименование</th>
                  <th>Описание</th>
                  <th>Изображение</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($tests as $test)
                <tr>
                  <td>{{ $test->id }}</td>
                  <td>{{ $test->name }}</td>
                  <td>{{ $test->description }}</td>
                  <td><img src="/images/{{ $test->image }}" height="60"></td>

                  <td><a class="btn btn-default btn-sm" href="<?php echo route('tests.edit', ['id' => $test->id]); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        <?php echo $tests->render(); ?>

@endsection
