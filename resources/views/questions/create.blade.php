@extends('layouts.main')

@section('content')
<h2 class="page-header">Добавление вопроса</h2>

<form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo route('questions.store', ['id' => $test_id]) ?>">
        {{ csrf_field() }}


        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="account" class="col-sm-2 control-label">Наименование</label>
            <div class="col-sm-4">
                <input class="form-control" id='question' type="text" value="{{ old('question') }}" name="question">
                    @if ($errors->has('question'))
                        <span class="help-block">
                            <strong>{{ $errors->first('question') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
            <label for="account" class="col-sm-2 control-label">Описание</label>
            <div class="col-sm-4">
                <textarea name="description" rows="8" cols="80">{{ old('description') }}</textarea>
                    @if ($errors->has('ends_at'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class="col-sm-2 control-label">Изображение</label>
            <div class="col-sm-4">
                <input id='image' type="file" value="" name="image">
            </div>
        </div>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-20">
      <button type="submit" class="btn btn-default">Сохранить</button>
    </div>
  </div>
</form>

@endsection
