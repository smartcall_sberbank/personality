@extends('layouts.main')

@section('content')
<h2 class="page-header">Редактирование теста #{{ $test->id }}</h2>

<form autocomplete="off" enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo route('tests.update', ['id' => $test->id]) ?>">
        {{ csrf_field() }}


<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="account" class="col-sm-2 control-label">Наименование</label>
    <div class="col-sm-4">
        <input class="form-control" id='name' type="text" value="{{ $test->name }}" name="name">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <label for="account" class="col-sm-2 control-label">Категория</label>
    <div class="col-sm-4">
        <input class="form-control" id='category_id' type="text" value="{{ $test->category_id }}" name="category_id">
            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="account" class="col-sm-2 control-label">Описание</label>
    <div class="col-sm-4">
        <textarea name="description" rows="8" cols="80">{{ $test->description }}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label for="image" class="col-sm-2 control-label">Изображение</label>
    <div class="col-sm-4">
        <input id='image' type="file" value="" name="image">
    </div>
</div>
@if ($test->image)
<div class="form-group">
    <img src="/images/{{ $test->image }}">
</div>
@endif


<h4 class="page-header">Вопросы</h4>
<div class="pull-right"><a class="btn btn-default" href="<?php echo route('questions.create',  ['id' => $test->id]) ?>" role="button">Добавить</a></div>

  <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Наименование</th>
          <th>Описание</th>
          <th>Изображение</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
          @foreach($test->questions as $question)
        <tr>
          <td>{{ $question->id }}</td>
          <td>{{ $question->question }}</td>
          <td>{{ $question->description }}</td>
          <td>@if($question->image)<img src="/images/{{ $question->image }}" height="60">@else &nbsp; @endif</td>

          <td><a class="btn btn-default btn-sm" href="<?php echo route('questions.edit', ['id' => $question->id]); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
          <a class="btn btn-default btn-sm" href="<?php echo route('questions.delete', ['id' => $question->id]); ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
        </tr>
        @endforeach

      </tbody>
    </table>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-2">
      <button type="submit" class="btn btn-default">Сохранить</button>
    </div>
  </div>
</form>

@endsection
