@extends('layouts.main')

@section('content')
<h2 class="page-header">Добавление варианта ответа</h2>

<form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo route('variants.store', ['id' => $question_id]) ?>">
        {{ csrf_field() }}


        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
            <label for="account" class="col-sm-2 control-label">Наименование</label>
            <div class="col-sm-4">
                <input class="form-control" id='text' type="text" value="{{ old('text') }}" name="text">
                    @if ($errors->has('text'))
                        <span class="help-block">
                            <strong>{{ $errors->first('text') }}</strong>
                        </span>
                    @endif
            </div>
        </div>



        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class="col-sm-2 control-label">Изображение</label>
            <div class="col-sm-4">
                <input id='image' type="file" value="" name="image">
            </div>
        </div>

  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-20">
      <button type="submit" class="btn btn-default">Сохранить</button>
    </div>
  </div>
</form>

@endsection
