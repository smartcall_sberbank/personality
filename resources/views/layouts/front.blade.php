<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Personality.kz</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="/css/swiper.min.css" rel="stylesheet" type="text/css"/>

	<link href="/css/styles.less"  rel="stylesheet/less" type="text/css"/>
	<link href="" rel="icon" type="image/png">
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/bootstrap/js/bootstrap.min.js"></script>
	<script src="/less.min.js" type="text/javascript"></script>
	<script src="/js/swiper.min.js" type="text/javascript"></script>
	<script src="/js/scripts.js" type="text/javascript"></script>
</head>
<body>
<header>
	<div class="wrapper">
		<div class="top-block">
			<div class="logo"><a href="/"><img src="/images/logo.png" alt=""></a></div>
			<div class="top-menu">
				<ul>
					<li><a href="#">О сайте</a></li>
					<li><a href="#">Результаты</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>

@yield('content')

<footer>
	<div class="wrapper">
		<div class="bottom-menu">
			<ul>
				<li><a href="#">Тесты</a></li>
				<li><a href="#">О сайте</a></li>
				<li><a href="#">Результаты</a></li>
			</ul>
		</div>
		<div class="copyright">© 2018 personality.kz</div>
	</div>
</footer>
</body>
</html>
