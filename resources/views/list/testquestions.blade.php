@extends('layouts.front')
@section('content')
<section class="page-wrap">
	<div class="wrapper">
		<ul class="breadcrumb">
			<li><a href="/">Главная</a></li>
      <li><a href="<?php echo route('category', ['id' => $test->category->id]); ?>">{{ $test->category->name }}</a></li>
			<li class="active">{{ $test->name }}</li>
		</ul>
		<ul class="social-networks">
			<li><a href="#"><img src="/images/icon_facebook.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon-twitter.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_pinterest.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_google.png" alt=""></a></li>
		</ul>
		<div class="gray-line"></div>

		<div class="row">

			<div class="col-xs-12 col-sm-8">
				<div class="test-block">

					<form action="/">
						<div class="swiper-container">
							<div class="swiper-pagination"></div>
							<div class="gray-line"></div>
							<div class="swiper-wrapper">
@foreach ($test->questions as $question)
								<div class="swiper-slide">
									<div class="test-title">{{ $question->question }}</div>
									<div class="btn-radio-multiple-text">

										<div class="row">
											@foreach($question->variants as $variant)
											<div class="col-xs-12">
												<div class="btn-radio">
													<input type="radio" id="test1-01" name="test1">
													<label for="test1-01" class="label-box swiper-next">
														@if($variant->image)<div class="image"><img src="/images/{{ $variant->image }}" alt=""></div>@endif
														{{ $variant->text }}</label>
												</div>
											</div>
											@endforeach

										</div>

										<a class="btn-back swiper-prev" href="#">назад</a>
									</div>
								</div>
								@endforeach
<div class="swiper-slide">
	<div class="test-begin-img">
		<img src="/images/uploads/bmw.jpg" alt="">
		<h3>Ваша марка - это BWM</h3>

		<p>BMW всегда всегда сочетала в себе экспрессию, динамизм, энергия и комфорт. </p>
	</div>
</div>

							</div>

							<!-- Add Arrows -->
							<div class="swiper-next"></div>
							<div class="swiper-prev"></div>
						</div>
					</form>
				</div>
			</div>


			<div class="col-xs-12 col-sm-4">
				<div class="sidebar-block">
					<div class="title">Другие тесты:</div>


          @foreach ($others as $other)
          <div class="row sidebar-el">
						<a href="<?php echo route('test', ['id' => $other->id]); ?>">
							<div class="col-xs-4">
								<div class="image"><img src="/images/{{ $other->image }}" alt=""></div>
							</div>
							<div class="col-xs-8">
								<div class="text">{{ $other->name }}</div>
							</div>
						</a>
					</div>
          @endforeach




				</div>
			</div>
		</div>


	</div>
</section>
@endsection
