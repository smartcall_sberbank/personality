@extends('layouts.front')

@section('content')
<section class="page-wrap">
	<div class="wrapper">
		<ul class="breadcrumb">
			<li><a href="/">Главная</a></li>
			<li class="active">{{ $category->name }}</li>
		</ul>
		<ul class="social-networks">
			<li><a href="#"><img src="/images/icon_facebook.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon-twitter.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_pinterest.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_google.png" alt=""></a></li>
		</ul>



		<div class="s-test-list popular-tests">
			<h3>{{ $category->name }}</h3>
			<div class="row">

        @foreach ($category->tests->take(10) as $test )
				<div class="col-xs-6 col-sm-3 col-md-2">
					<div class="test-el">
						<div class="image"><a href="<?php echo route('test', ['id' => $test->id]); ?>"><img src="/images/{{ $test->image }}" alt=""></a></div>
						<div class="title"><a href="<?php echo route('test', ['id' => $test->id]); ?>">{{ $test->name }}</a></div>
						<div class="desc">{{ $test->description }}</div>
					</div>
				</div>

        @endforeach

			</div>

		</div>


	</div>
</section>



@endsection
