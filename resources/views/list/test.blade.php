@extends('layouts.front')
@section('content')
<section class="page-wrap">
	<div class="wrapper">
		<ul class="breadcrumb">
			<li><a href="/">Главная</a></li>
      <li><a href="<?php echo route('category', ['id' => $test->category->id]); ?>">{{ $test->category->name }}</a></li>
			<li class="active">{{ $test->name }}</li>
		</ul>
		<ul class="social-networks">
			<li><a href="#"><img src="/images/icon_facebook.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon-twitter.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_pinterest.png" alt=""></a></li>
			<li><a href="#"><img src="/images/icon_google.png" alt=""></a></li>
		</ul>
		<div class="gray-line"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="test-begin">
					<div class="test-begin-title">{{ $test->name }}</div>
					<div class="written-by">Составил <span>Джон Смит</span></div>
					<div class="test-begin-img">
						<img src="/images/{{ $test->image }}" alt="">
					</div>
					<a href="/vk/authorize" class="btn-dark-green">Пройти тест</a>


				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<div class="sidebar-block">
					<div class="title">Другие тесты:</div>


          @foreach ($others as $other)
          <div class="row sidebar-el">
						<a href="<?php echo route('test', ['id' => $other->id]); ?>">
							<div class="col-xs-4">
								<div class="image"><img src="/images/{{ $other->image }}" alt=""></div>
							</div>
							<div class="col-xs-8">
								<div class="text">{{ $other->name }}</div>
							</div>
						</a>
					</div>
          @endforeach




				</div>
			</div>
		</div>


	</div>
</section>
@endsection
