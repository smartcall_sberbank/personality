<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'ListController@index')->name('home');

Route::get('/test/{id}', 'ListController@test')->name('test');

Route::get('/test/questions/{id}', 'ListController@testQuestions')->name('test.questions');

Route::get('/category/{id}', 'ListController@category')->name('category');

Route::prefix('admin')->group(function(){

  Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');


    Route::get('vkusers', 'VkUsersController@index')->name('vkusers.all');
    Route::get('vkusers/groups/{id}', 'VkUsersController@groups')->name('vkusers.groups');
    Route::get('vkusers/friends/{id}', 'VkUsersController@friends')->name('vkusers.friends');

    Route::get('vkusers/groupsnow/{id}', 'VkUsersController@groupsnow')->name('vkusers.groupsnow');

    Route::get('tests', 'TestsController@index')->name('tests.all');
    Route::get('tests/create', 'TestsController@create')->name('tests.create');
    Route::post('tests/store', 'TestsController@store')->name('tests.store');
    Route::get('tests/edit/{id}', 'TestsController@edit')->name('tests.edit');
    Route::get('tests/destroy/{id}', 'TestsController@destroy')->name('tests.destroy');
    Route::post('tests/update/{id}', 'TestsController@update')->name('tests.update');

    Route::get('questions/edit/{id}', 'QuestionsController@edit')->name('questions.edit');
    Route::get('questions/create/{id}', 'QuestionsController@create')->name('questions.create');
    Route::post('questions/store/{id}', 'QuestionsController@store')->name('questions.store');
    Route::post('questions/update/{id}', 'QuestionsController@update')->name('questions.update');
    Route::get('questions/delete/{id}', 'QuestionsController@delete')->name('questions.delete');

    Route::get('variants/edit/{id}', 'VariantsController@edit')->name('variants.edit');
    Route::get('variants/create/{id}', 'VariantsController@create')->name('variants.create');
    Route::post('variants/store/{id}', 'VariantsController@store')->name('variants.store');
    Route::post('variants/update/{id}', 'VariantsController@update')->name('variants.update');
    Route::get('variants/delete/{id}', 'VariantsController@delete')->name('variants.delete');
});
